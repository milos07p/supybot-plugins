###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.ircmsgs as ircmsgs
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('AntiHL')

@internationalizeDocstring
class AntiHL(callbacks.Plugin):
    """Annoyed by the motherfuckers that highlight "for fun"?
    Load me and watch them burn."""
    threaded = True

    def doPrivmsg(self, irc, msg):
        # It is useless to check on private messages.
        # Would also raise a KeyError when searching for the channel
        # in the irc.state.channels
        if msg.args[0] == irc.nick:
            irc.noReply()
            return
        channel = msg.args[0]
        c = irc.state.channels[channel]

        if not self.registryValue('enabled', msg.args[0]):
            return
        # Check the bot's status
        if not ((irc.nick in c.ops) or (irc.nick in c.halfops)):
            return # Damn.

        # These should be allowed
        if (msg.nick in c.ops) or (msg.nick in c.halfops) or \
            ((msg.nick in c.voices) if self.registryValue('exceptVoices', channel) \
            else False): return

        message   = msg.args[1].lower()
        nicks     = c.users
        nicks_num = len(nicks)

        if nicks_num <= 5:
            # This is probably an 'allowed' highlight.
            # Will be customizable in the future
            return

        required = (nicks_num - 2) if nicks_num < 15 else 15
        count    = 0
        for n in nicks:
            n = n.lower()
            if n in message:
                count += 1
            if count > required: break

        if required <= count:
            if self.registryValue('banOnHL'):
                # Ban the motherfucker.
                host = '*!*@' + msg.host
                irc.sendMsg(ircmsgs.ban(channel, host))
            irc.sendMsg(ircmsgs.kick(channel, msg.nick))

Class = AntiHL


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
