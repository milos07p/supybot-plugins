###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.ircdb as ircdb
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircmsgs as ircmsgs
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Broadcast')

@internationalizeDocstring
class Broadcast(callbacks.Plugin):
    """Add the help for "@plugin help Broadcast" here
    This should describe *how* to use this plugin."""
    pass

    def broadcast(self, irc, msg, args, text):
        """<message>
        Sends the message to all channels the bot is in.
        """
        if self.registryValue('allowAdmin'):
            capability = 'admin'
        else:
            capability = 'broadcast'

        if not ircdb.checkCapability(msg.prefix, capability):
            irc.errorNoCapability(capability, Raise=True)

        # Preparation
        try:
            user = ircdb.users.getUser(msg.prefix).name
        except KeyError:
            user = 'non-registered user'

        if msg.args[0] == irc.nick:
            channel = 'private'
        else:
            channel = msg.args[0]

        message = self.registryValue('message')
        message = message.replace('&nick', msg.nick).replace('&ident',
                   msg.user).replace('&host', msg.host).replace('&channel',
                   channel).replace('&user', user).replace('&message', text)

        # This way, registry value is checked once, and not 1*channels times.
        if self.registryValue('sendToLobotomized'):
            channels = [c for c in irc.state.channels]
        else:
            channels = []
            for c in irc.state.channels:
                c = ircdb.channels.getChannel(channel)
                if not c.lobotomized:
                    channels.append(c)

        # Commence teh spamzzor
        for channel in channels:
            irc.queueMsg(ircmsgs.privmsg(channel, message))

    broadcast = wrap(broadcast, ['text'])

Class = Broadcast


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
