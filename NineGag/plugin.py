###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import json
import urllib2

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('NineGag')

class Gag:
    def __init__(self, js):
        self.id    = js['id']
        self.title = js['title']
        self.image = js['image']['big']
        self.votes = js['votes']
        self.age   = js['time']

@internationalizeDocstring
class NineGag(callbacks.Plugin):
    """Add the help for "@plugin help NineGag" here
    This should describe *how* to use this plugin."""
    pass

    def _getGag(self, i):
        api = urllib2.urlopen('http://infinigag.com/api.json?id=%s' % i)
        data = json.load(api)
        api.close()
        return Gag(data)

    def doPrivmsg(self, irc, msg):
        if(self.registryValue('enabled',msg.args[0])):
            if (msg.args[1].find('http://9gag.com/gag/') != -1):
                for word in msg.args[1].split():
                    if word.startswith('http://9gag.com/gag/'):
                        i = word.split('/gag/')[1]
                        try:
                            gag = self._getGag(i)
                        except ValueError:
                            # Invalid gag ID
                            return

                        msg = '0,14 9GAG  Title: %s [%s] - Votes: ' \
                                '%s - Posted: %s - Image URL: %s' % \
                                  (gag.title, gag.id, gag.votes, gag.age, gag.image)
                        irc.reply(msg)

Class = NineGag


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
