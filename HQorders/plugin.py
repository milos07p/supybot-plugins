###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import json
import time
import urllib2
from datetime import datetime

import supybot.conf as conf
import supybot.ircdb as ircdb
import supybot.utils as utils
from supybot.commands import *
import supybot.ircmsgs as ircmsgs
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('HQorders')

# Wrapper
class Priority:
    def __init__(self, number, added_by, battle, note):
        self.number   = number
        self.added_by = added_by
        self.battle   = battle
        self.note     = note
        self.added_on = time.time()


@internationalizeDocstring
class HQorders(callbacks.Plugin):
    """Add the help for "@plugin help HQorders" here
    This should describe *how* to use this plugin."""
    threaded = True

    priorities = [None for x in range(5)]

    def _verifyBattle(self, id):
        # Ensuring the data is valid
        id = int(id)

        api = urllib2.urlopen('http://e-sim.org/apiFights.html?battleId=%s&roundId=1' % id)
        data = json.load(api)
        api.close()
        if 'error' in data:
            raise ValueError

    def _announce(self, irc, message):
        for channel in irc.state.channels:
            irc.queueMsg(ircmsgs.privmsg(channel, message))

    def prios(self, irc, msg, args):
        """takes no arguments
        Lists priorities set.
        """
        count = 0
        for prio in self.priorities:
            count += 1
            if prio != None:
                irc.reply("PRIO %s - http://e-sim.org/battle.html?id=%s %s| set %s." % (prio.number, prio.battle, (prio.note+' ' if prio.note else ''),
                                      utils.timeElapsed(prio.added_on - time.time())))
            else:
                irc.reply("PRIO %s not set." % count)
    prios = wrap(prios)

    # Handling priorities
    def prio(self, irc, msg, args, action, number, battle, text):
        """{view|set|del} {Priority (1-5)} [battle] [note]
        Battle ID needed only when setting priority, additional note
        is optional.
        """

        # Let's do this here to save ourselves from doing it later.
        number = int(number)
        action = action.lower()

        if action in ['set', 'del', 'announce']:
            allowed = ircdb.checkCapability(msg.prefix, 'hq')
            if not allowed:
                irc.reply("Get the fuck out.")
                return

        if action == "set":
            if battle:
                try:
                    self._verifyBattle(battle)
                except ValueError:
                    irc.error("Invalid battle ID.", Raise=True)

                new = Priority(number, msg.nick, battle, text)
                self.priorities[number-1] = new
                irc.reply("Priority %s set to http://e-sim.org/battle.html?id=%s%s" % (number, battle, (' with note: %s'%text if text else '')))
                self._announce(irc, "HQ announcement! PRIO %s changed to http://e-sim.org/battle.html?id=%s%s" % (number, battle, (' | %s'%text if text else '')))
            else:
                irc.error(Raise=True)

        elif action == "del":
            if battle or text:
                irc.error(Raise=True)
            elif (self.priorities[number-1] == None):
                irc.error("Priority %s is not set." % number, Raise=True)
            else:
                self.priorities[number-1] = None
                irc.reply("Priority %s cleared." % number)

        elif action == "view":
            if battle or text:
                irc.error(Raise=True)
            else:
                prio = self.priorities[number-1]
                if not prio:
                    irc.error("Priority %s is not set." % number, Raise=True)
                irc.reply("Priority %s - http://e-sim.org/battle.html?id=%s - set by: %s %s%s" % (prio.number, prio.battle, prio.added_by,
                                      utils.timeElapsed(prio.added_on - time.time(), short=True),
                                      (' | %s' % prio.note) if prio.note else ''))

        #elif action == "announce":
        #    if battle or text:
        #        irc.error(Raise=True)
        #    else:
        #        prio = self.priorities[number-1]
        #        if prio == None:
        #            irc.error("Priority %s is not set." % number, Raise=True)
        #        irc.reply("Announcing priority %s in %s channels.", (number, len(irc.state.channels)))
        #        message = "PRIO %s - http://e-sim.org/battle.html?id=%s - HIT HERE BITCHES" % (prio.number, prio.battle)
        #        for channel in irc.state.channels:
        #            irc.queueMsg(ircmsgs.privmsg(channel, message))

    prio = wrap(prio, [('literal', ['set', 'del', 'view']), # 'announce'
                       ('literal', ['1', '2', '3', '4', '5']),
                       optional('something'), optional('text')])


Class = HQorders


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
