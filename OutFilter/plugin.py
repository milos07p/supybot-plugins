###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.ircmsgs as ircmsgs
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('OutFilter')

@internationalizeDocstring
class OutFilter(callbacks.Plugin):
    """The plugin checks the channels before sending messages to see if
    they have +m/+c and if the bot has status to counter it."""
    pass

    def outFilter(self, irc, msg):
        if msg.args[0][0] != '#': return msg 
        channel = msg.args[0]
        if self.registryValue("enabled", channel) and msg.command == 'PRIVMSG':
            c = irc.state.channels[channel]
            allowed = irc.nick in c.ops or \
                      irc.nick in c.halfops or \
                      irc.nick in c.voices
            if not allowed:
                if 'm' in c.modes:
                    self.log.info('Destroying message that should be sent to %s, ' \
                                  'the channel is moderated and I\'m not +vho.' % msg.args[0])
                    return None
                elif 'c' in c.modes:
                          s = msg.args[1]
                          s = ircutils.stripFormatting(s)
                          msg = ircmsgs.privmsg(channel, s)
        return msg

Class = OutFilter


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
