###
# Copyright (c) 2012, Digital_Lemon
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import time
from local import pastebin

import supybot.utils as utils
from supybot.commands import *
import supybot.ircmsgs as ircmsgs
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Log2Pastebin')

class Logging(object):
    def __init__(self, nick):
        self.nick = nick
        self.active = True
        self.started = time.time()
        self.messages = []

@internationalizeDocstring
class Log2Pastebin(callbacks.Plugin):
    """Add the help for "@plugin help Log2Pastebin" here
    This should describe *how* to use this plugin."""
    pass

    def __init__(self, irc):
        self.__parent = super(Log2Pastebin, self)
        self.__parent.__init__(irc)
        self.logging = {}
        self.client = pastebin.Client(self.registryValue('apiKey'))

    def pastebin(self, irc, msg, args, action, channel):
        """{start|stop|upload} [#channel]
        Controls the Log2Pastebin logging engine.
        """
        action = action.lower()
        channel = channel.lower()

        if channel in self.logging:
            if self.logging[channel].active:
                self.logging[channel].messages.pop()

        if action == 'start':
            if channel in self.logging:
                if not self.logging[channel].active:
                    irc.error('Logging in %s was stopped, use "@pastebin upload" to pastebin it.' % msg.args[0], Raise=True)
                irc.error('Logging in %s is already active.' % msg.args[0], Raise=True)
            self.logging[channel] = Logging(msg.nick)
            irc.reply('Logging started by %s.' % msg.nick)
            self.log.info('Channel logging started for %s by %s.' % (channel, msg.prefix))

        elif action == 'stop':
            if not channel in self.logging:
                irc.error('Logging in %s is not active.' % msg.args[0], Raise=True)
            elif not self.logging[channel].active:
                irc.error('Logging in %s was stopped, use "@pastebin upload" to pastebin it.' % msg.args[0], Raise=True)

            self.logging[channel].active = False
            log = self.logging[channel]
            count = len(log.messages)
            irc.reply('Logging stopped by %s, %s lines were collected in %s.'% (msg.nick, count,
                                                    utils.timeElapsed(time.time() - log.started)))

        elif action == 'view':
            if not channel in self.logging:
                irc.error('Logging in %s is not active.' % channel, Raise=True)
            for line in self.logging[channel].messages:
                irc.reply(line)

        elif action == 'upload':
            if channel in self.logging:
                if self.logging[channel].active:
                    irc.error('Logging is still active, use "@pastebin stop" to stop it first!', Raise=True)
                logger = self.logging[channel]
                title = '%s at %s' % (msg.args[0], irc.network)
                text = '\n'.join(logger.messages)
                link = self.client.post(title, text)
                irc.reply('Pastebin link: %s' % link)
                del self.logging[channel]

    pastebin = wrap(pastebin, [('literal', ['start', 'stop', 'view', 'upload']), 'channel'])

    # Events
    def inFilter(self, irc, msg):
        channel = msg.args[0].lower()
        if (channel in self.logging and self.logging[channel].active) or msg.command == 'QUIT':
            self._handleMessage(irc, msg)
        return msg

    def _getNickStatus(self, irc, msg):
        c = irc.state.channels[msg.args[0]]
        if msg.nick in c.ops:
            return '@'+msg.nick
        elif msg.nick in c.halfops:
            return '%'+msg.nick
        elif msg.nick in c.voices:
            return '+'+msg.nick
        return msg.nick

    def _handleMessage(self, irc, msg):
        cmd = msg.command
        if cmd == 'PRIVMSG':
            nick = self._getNickStatus(irc, msg)
            message = '<%s> %s' % (nick, msg.args[1])
        elif cmd == 'JOIN':
            nick = msg.nick
            message = '*** %s has joined %s' % (nick, msg.args[0])
        elif cmd == 'PART':
            nick = msg.nick
            message = '*** %s has left %s%s' % (nick, msg.args[0], (': %s'%msg.args[1]) if msg.args[1] else '')
        elif cmd == 'KICK':
            nick = msg.nick
            kicked = msg.args[1]
            message = '*** %s was kicked from %s by %s: %s' % (kicked, nick, msg.args[0], msg.args[2])
        elif cmd == 'MODE':
            nick = msg.nick
            message = '*** %s has set mode %s' % (nick, ' '.join(msg.args[1:]))
        elif cmd == 'QUIT':
            nick = msg.nick
            for c in self.logging:
                if not c.active:
                    continue
                if nick in irc.state.channels[c].users:
                    message = '*** %s has quit IRC: %s' % (nick, msg.args[0])
                    self.logging[c].messages.append(message)
                    return
        elif cmd == 'NOTICE':
            nick = self._getNickStatus(irc, msg)
            message = '*%s* %s' % (nick, msg.args[1])
        else:
            self.log.warning('Interrupted an unknown message: %s' % msg.__repr__)
            return
        # Saving!
        channel = msg.args[0].lower()
        self.logging[channel].messages.append(message)


Class = Log2Pastebin


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
