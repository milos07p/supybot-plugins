import urllib2
from urllib2 import quote

class Client:
    def __init__(self, key):
        self.api_key = key

    def post(self, title, text, syntax=None):
        data = 'api_option=paste&api_user_key=&api_paste_private=1' \
                + '&api_paste_name=' + quote(title) + '&api_paste_expire_date=N' + \
                '&api_dev_key=' + self.api_key + '&api_paste_code=' + quote(text)

        api = urllib2.urlopen('http://pastebin.com/api/api_post.php', data)
        response = api.read()
        api.close()
        return response
